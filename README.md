A Jenkins build master with additional packages found to be required.

The reason this exists is because of some jobs we run:

1. Some require the use of GhostScript - which on Ubuntu also requires an extra symbolic link for a shared library
2. The loading in of the Docker command line tool downloaded from Docker themselves, version "latest".
3. The loading in of `jq` (a command line JSON parser) as builds may wish to unpack JSON payloads that triggered them.

The Docker CLI is needed because during 2015 Docker switched to shipping a command line tool dynamically linked against a number of shared libraries and bind-mounting these into this container is painful.
